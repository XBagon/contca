use amethyst::assets::{AssetStorage, Loader};
use amethyst::core::cgmath::{Vector3, Vector2, Matrix4};
use amethyst::core::transform::{GlobalTransform, Transform};
use amethyst::ecs::prelude::{Component, DenseVecStorage, System};
use amethyst::input::{is_close_requested, is_key_down};
use amethyst::prelude::*;
use amethyst::renderer::{
    Camera, PngFormat, Projection, Sprite, SpriteRender, Texture, TextureHandle,
    VirtualKeyCode, Transparent, SpriteSheetHandle, TextureCoordinates, SpriteSheet, MaterialTextureSet
};
use amethyst::StateEvent;
use amethyst_rhusics::rhusics_ecs::physics2d::Circle;
use components::*;
use rand;

pub struct Simulation;

impl<'a, 'b> SimpleState<'a, 'b> for Simulation {
    fn on_start(&mut self, data: StateData<GameData>) {
        let world = data.world;
        world.register::<Cell>();

        let sprite_sheet = load_sprite_sheet(world);
        initialize_cells(world,sprite_sheet);
        initialize_camera(world);
    }

    fn handle_event(&mut self, _: StateData<GameData>, event: StateEvent<()>) -> SimpleTrans<'a, 'b> {
        if let StateEvent::Window(event) = event {
            if is_close_requested(&event) || is_key_down(&event, VirtualKeyCode::Escape) {
                Trans::Quit
            } else {
                Trans::None
            }
        }else {
            Trans::None
        }
    }

    fn update(&mut self, data: &mut StateData<GameData>) -> SimpleTrans<'a, 'b> {
        data.data.update(&data.world);
        Trans::None
    }
}

const ARENA_WIDTH: f32 = 1920.0;
const ARENA_HEIGHT: f32 = 1080.0;

fn initialize_camera(world: &mut World) {
    world.create_entity()
        .with(Camera::from(Projection::orthographic(
            0.0,
            ARENA_WIDTH,
            ARENA_HEIGHT,
            0.0,
        )))
        .with(GlobalTransform(
            Matrix4::from_translation(Vector3::new(0.0, 0.0, 1.0)).into()
        ))
        .build();
}

fn load_sprite_sheet(world: &mut World) -> SpriteSheetHandle {
    const SPRITESHEET_SIZE: (f32, f32) = (10.0, 10.0);
    const CELL_HEIGHT: f32 = 10.0;
    const CELL_WIDTH: f32 = 10.0;
    // Load the sprite sheet necessary to render the graphics.
    // The texture is the pixel data
    // `sprite_sheet` is the layout of the sprites on the image

    // `texture_handle` is a cloneable reference to the texture
    let texture_handle = {
        let loader = world.read_resource::<Loader>();
        let texture_storage = world.read_resource::<AssetStorage<Texture>>();
        loader.load(
            "textures/cell.png",
            PngFormat,
            Default::default(),
            (),
            &texture_storage,
        )
    };
    // `texture_id` is a application defined ID given to the texture to store in the `World`.
    // This is needed to link the texture to the sprite_sheet.
    let texture_id = 0;
    let mut material_texture_set = world.write_resource::<MaterialTextureSet>();
    material_texture_set.insert(texture_id, texture_handle);

    // Create the sprite for the paddles.
    //
    // Texture coordinates are expressed as a proportion of the sprite sheet's dimensions between
    // 0.0 and 1.0, so they must be divided by the width or height.
    //
    // In addition, on the Y axis, texture coordinates are 0.0 at the bottom of the sprite sheet and
    // 1.0 at the top, which is the opposite direction of pixel coordinates, so we have to invert
    // the value by subtracting the pixel proportion from 1.0.
    let tex_coords = TextureCoordinates {
        left: 0.0,
        right: CELL_WIDTH / SPRITESHEET_SIZE.0,
        bottom: 1.0 - CELL_HEIGHT / SPRITESHEET_SIZE.1,
        top: 1.0,
    };
    let paddle_sprite = Sprite {
        width: CELL_WIDTH,
        height: CELL_HEIGHT,
        offsets: [CELL_WIDTH / 2.0, CELL_HEIGHT / 2.0],
        tex_coords,
    };

    // Collate the sprite layout information into a sprite sheet
    let sprite_sheet = SpriteSheet {
        texture_id,
        sprites: vec![paddle_sprite],
    };

    let sprite_sheet_handle = {
        let loader = world.read_resource::<Loader>();
        let sprite_sheet_store = world.read_resource::<AssetStorage<SpriteSheet>>();
        loader.load_from_data(sprite_sheet, (), &sprite_sheet_store)
    };

    sprite_sheet_handle
}

struct Cell {
    size : f32
}

impl Component for Cell {
    type Storage = DenseVecStorage<Self>;
}

impl Cell {
    fn new(size: f32) -> Self{
        Cell{
            size
        }
    }
}

fn initialize_cells(world: &mut World, sprite_sheet: SpriteSheetHandle){
    initialize_cell(world, sprite_sheet.clone(), [1.0,0.0,0.0,1.0], Vector2::new(50.0,50.0));
    initialize_cell(world, sprite_sheet.clone(), [0.0,1.0,0.0,1.0], Vector2::new(25.0,50.0));
    initialize_cell(world, sprite_sheet.clone(), [0.0,0.0,1.0,1.0], Vector2::new(75.0,50.0));

    for i in 0..1000 {
        initialize_cell_randomly(world, sprite_sheet.clone());
    }
}

fn initialize_cell(world : &mut World, sprite_sheet: SpriteSheetHandle, color : [f32; 4], position : Vector2<f32>){
    let sprite = SpriteRender {
        sprite_sheet,
        sprite_number: 0,
        flip_horizontal: false,
        flip_vertical: false,
        mul_color: color
    };

    let mut transform = Transform::default();
    transform.translation = position.extend(0.0);

    world.create_entity()
        .with(sprite)
        .with(Transparent)
        .with(Cell::new(1.0))
        .with(GlobalTransform::default())
        .with(transform)
        .with(MoveRandomly{ speed : 5.0 })
        //.with(Circle::new(1.0))
        .build();
}

fn initialize_cell_randomly(world : &mut World, sprite_sheet: SpriteSheetHandle){
    let sprite = SpriteRender {
        sprite_sheet,
        sprite_number: 0,
        flip_horizontal: false,
        flip_vertical: false,
        mul_color: [rand::random(),rand::random(),rand::random(),1.0]
    };

    let mut transform = Transform::default();
    transform.translation = Vector3::new(rand::random::<f32>()*ARENA_WIDTH,rand::random::<f32>()*ARENA_HEIGHT,0.0);

    world.create_entity()
        .with(sprite)
        .with(Transparent)
        .with(Cell::new(1.0))
        .with(GlobalTransform::default())
        .with(transform)
        .with(MoveRandomly{ speed : 5.0 })
        //.with(Circle::new(1.0))
        .build();
}