use amethyst::core::timing::Time;
use amethyst::core::transform::Transform;
use amethyst::ecs::prelude::{Join, Read, Write, ReadStorage, System, WriteStorage};
use components::MoveRandomly;
use rand::{self, SeedableRng, Rng, IsaacRng};

pub struct MoveRandomlySystem{
    pub rng : IsaacRng,
}

impl<'s> System<'s> for MoveRandomlySystem{
    type SystemData = (
        ReadStorage<'s, MoveRandomly>,
        WriteStorage<'s, Transform>,
        Read<'s, Time>
    );

    fn run(&mut self, (moves, mut locals, time) : Self::SystemData) {
        for (mov, local) in (&moves, &mut locals).join() {
            local.translation[0] += self.rng.gen_range::<f32>(-1.0,1.0) * mov.speed * time.delta_seconds();
            local.translation[1] += self.rng.gen_range::<f32>(-1.0,1.0) * mov.speed * time.delta_seconds();
        }
    }
}