use amethyst::ecs::prelude::{Component, DenseVecStorage};

pub struct MoveRandomly {
    pub speed : f32
}

impl Component for MoveRandomly {
    type Storage = DenseVecStorage<Self>;
}