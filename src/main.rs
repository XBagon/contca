extern crate amethyst;
extern crate amethyst_rhusics;
extern crate core;
extern crate rand;

mod simulation;
mod components;
mod systems;

use amethyst::prelude::*;
use amethyst::renderer::{DisplayConfig, DrawSprite, Event, Pipeline, PosTex, RenderBundle, Stage, VirtualKeyCode, ALPHA, ColorMask, DepthMode};
use amethyst::core::{transform::TransformBundle,frame_limiter::FrameRateLimitStrategy};
use amethyst_rhusics::DefaultPhysicsBundle2;
use simulation::Simulation;
use std::time::Duration;
use systems::*;

use rand::{SeedableRng, Rng};

fn main() -> amethyst::Result<()> {
    amethyst::start_logger(Default::default());

    let path = "./resources/display_config.ron";
    let config = DisplayConfig::load(&path);

    let pipe = Pipeline::build().with_stage(
        Stage::with_backbuffer()
            .clear_target([0.0, 0.0, 0.0, 1.0], 1.0)
            .with_pass(DrawSprite::new().with_transparency(
                ColorMask::all(),
                ALPHA,
                None,
            ))
    );

    let game_data = GameDataBuilder::default()
        .with_bundle(TransformBundle::new())?
        .with_bundle(RenderBundle::new(pipe, Some(config))
            .with_sprite_sheet_processor()
            .with_sprite_visibility_sorting(&["transform_system"]))?
        .with_bundle(DefaultPhysicsBundle2::<()>::new().with_spatial())? //TODO have Collision implementor instead of ()
        .with(MoveRandomlySystem { rng: rand::IsaacRng::from_seed(*b"agfgr40)OPf67#sasfwr53/@@as83pg3") }, "move_randomly_system", &[]);


    let mut game = Application::build("./", Simulation)?
        .with_frame_limit(
            FrameRateLimitStrategy::SleepAndYield(Duration::from_millis(2)),
            144,
        )
        .build(game_data)?;
    game.run();

    Ok(())
}